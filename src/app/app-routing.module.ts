import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TestComponent } from './test/test.component';
import { HiComponent } from './hi/hi.component';
import { HelloComponent } from './hello/hello.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { ObserveComponent } from './observe/observe.component';

const routes: Routes = [
  {path:'' , component:HiComponent},
  {path:"test",component:TestComponent},
  {path:'hello',component:HelloComponent},
  {path:'welcome',component:WelcomeComponent},
  { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule) },
  //{path:'',redirectTo:'/hi',pathMatch:'full'}
  {path:'observe',component:ObserveComponent}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
