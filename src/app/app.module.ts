import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MatButtonModule} from '@angular/material/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TestComponent } from './test/test.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HiComponent } from './hi/hi.component';
import { HelloComponent } from './hello/hello.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { MathsService } from './services/maths.service';
import { ObserveComponent } from './observe/observe.component';


@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    HiComponent,
    HelloComponent,
    WelcomeComponent,
    ObserveComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatButtonModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [MathsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
