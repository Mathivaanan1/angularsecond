import { Component, OnInit } from '@angular/core';
import { MathsService } from '../services/maths.service';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.css']
})
export class HelloComponent implements OnInit{


constructor(public _maths:MathsService){}

  ngOnInit(): void {
    
  }
  increase(){
    this._maths.addone();
  }

}
