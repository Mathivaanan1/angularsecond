import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MathsService } from '../services/maths.service';

@Component({
  selector: 'app-hi',
  templateUrl: './hi.component.html',
  styleUrls: ['./hi.component.css']
})
export class HiComponent implements OnInit {



btntoggle:boolean=true;

class_bind="red";


constructor(private router:Router,public _maths:MathsService){}
  ngOnInit(): void {

    setTimeout(()=>{
      this.btntoggle=false;
    },2000);

    
  }
  ontoggle(){
    this.class_bind="green"
  }
    
  gotohello(){
    this.router.navigate(['/hello']);
  }

}
