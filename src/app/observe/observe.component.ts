import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-observe',
  templateUrl: './observe.component.html',
  styleUrls: ['./observe.component.css']
})
export class ObserveComponent  implements OnInit{

  constructor(){}
ngOnInit(): void {
  
  const myobj$ =new Observable(obs=>{
      
    console.log('start');
    obs.next('10');
    console.log('end');
});

myobj$.subscribe(sub=>{
  console.log(sub);
});

}
}
