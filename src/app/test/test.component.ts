import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MathsService } from '../services/maths.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  @Input() subjectchild: any;

@Output() private numbergenerate=new EventEmitter<Number>();

constructor(public _maths:MathsService){}
  ngOnInit(): void {
    

}
generatenumber(){
  const randomnumber=Math.random();
  this.numbergenerate.emit(randomnumber);
}

}